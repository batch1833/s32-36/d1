const Course = require("../models/Course")
const auth = require("../auth");
//Create a new course
/*
	Steps:
	1. Create a new course object using the mongoose model and the information from the request body.
	2. Save the new user to out database.
*/

module.exports.addCourse = (reqBody) =>{
	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		slots: reqBody.slots
	})

	return newCourse.save().then((course, error) =>{
		if(error){
			return false
		}
		else{
			return true
		}
	})
}

//Retrieve all courses
/*
	Steps:
	1. Retrieve all the courses from the database
*/
module.exports.getAllCourses = () => {
					// if empty i-return lahat ng properties
	return Course.find({}).then(result => result);
}

// Retrieve all Active Courses
/*
	Steps:
	1. Retrieve all the courses from the database with the property of "isActive" to true.
*/
							//none para ma get all isActive = true
module.exports.getAllActive = () =>{
	return Course.find({isActive:true}).then(result => result);
}

//Retrieving a specific course
/*
	Step:
	1. Retrieve the course that matches the course ID provided from the URL(kay params)
*/

module.exports.getCourse = (courseId) => {
	return Course.findById(courseId).then(result => result);
}

// Update a course
/*
	Steps:
	1. Create a variable "updatedCourse" which will contains the information retrieved from the request body.
	2. Find and update the course using the courseId retreived from request params/url property and the variable "updatedCourse" containing the information from the request body. 
*/

module.exports.updateCourse = (courseId, reqBody) =>{
	//Specify the fileds/properties to be updated(eto lng mga update na need to update)// pwede ma update yung nasa baba
	let updatedCourse ={
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		slots: reqBody.slots
	}

	//using this no need .save
	//Syntax: findByIdAndUpdate(documentId, updatesToBeApplied)

	return Course.findByIdAndUpdate(courseId, updatedCourse).then((courseUpdate, error)=>{
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}

//Archive a course
/*
	Mini activity:
	1. Create a "archiveCourse" function that will change the status of an active course to inactive using the "isActive" property.
	2. return true, if the course is set to inactive, and false if encountered an error.
	3. Once done, send a screenshot of your postman in the batch hangouts.
*/

//Route to Achive a course

module.exports.archiveCourse = (courseId) =>{
	let updateActiveField = {
		isActive : false
	}

	return Course.findByIdAndUpdate(courseId, updateActiveField).then((isActive, error) =>{
		// Course is not archived
		if(error){
			return false;
		}
		// Course archived successfully
		else{
			return true
		}
	})
}

