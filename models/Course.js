/*
Course Properties:
name
description
slots
price
isActive
enrollees
*/

const mongoose = require("mongoose");

const courseSchema = new mongoose.Schema({
	name:{
		type: String, 
		required: [true, "Course name is required"]
	},
	description: {
		type: String,
		required: [true, "Description is required"]
	},
	price:{
		type: Number,
		required: [true, "Price is required"]
	},
	slots:{
		type: Number,
		required: [true, "Slot is required"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createOn: {
		type: Date,
		default: new Date()
	},
	enrollees: [
		{
			userId:{
				type: String,
				required: [true, "UserId is Required"]
			},
			enrolledOn:{
				type: Date,
				default: new Date()
			}
		}
	]
})

module.exports = mongoose.model("Course", courseSchema);

//Mini Activity

/*
Create the User model that will be used in creating the user records and storing information relevant to our application.

1. Create a "User.js" file to store the schema for our users.
2. Create a User schema with the following properties:
    - firstName - String
    - lastName - String
    - email -String
    - password - String
    - isAdmin - String
    - mobileNo - String
    - enrollments - Array of objects
        - courseId - String
        - enrolledOn - Date (Default value - new Date object)
        - status - String (Default value - Enrolled)
3. Make sure that all the fields are required (except fields with default value) to ensure consistency in the user information being stored in our database.
4. Make sure to export your User model.

*/